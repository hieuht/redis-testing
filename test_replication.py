import redis

master = ("192.168.12.20", "foobared")
slaves = [("192.168.12.21", "foobared")]
test_key = "test_key"
test_value = "test_value"
try:
    ip_master, auth = master    
    print "Redis Master: %s" % ip_master    
    master_redis = redis.Redis(host= ip_master, password= auth)
    master_redis.set(test_key, test_value)
    master_test_value = master_redis.get(test_key)
except redis.exceptions.ConnectionError:
    print "Can not connect to Redis Master"
except redis.exceptions.ResponseError:
    print "Wrong Pass On Master"
    
for slave in slaves:
    try:
        ip_slave, auth = slave
        print "Slave: %s" % ip_slave
        slave_redis = redis.Redis(host= ip_slave, password= auth)
        print " - Slave Of: %s" % slave_redis.info()['master_host']        
        if 'yes' == slave_redis.config_get('slave-read-only').values()[0]:
            print " - Slave Read Only: YES"
        else:
            print " - Slave Read Only: NO"
        slave_test_value = slave_redis.get(test_key)
        if slave_test_value == master_test_value:
            print " - Sync: YES"
        else:
            print " - Sync: NO"
    except redis.exceptions.ConnectionError:
        print "Can not connect to Redis Slave"
    except redis.exceptions.ResponseError:
        print "Wrong Pass On Slave %s" % ip_slave

        